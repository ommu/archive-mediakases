<?php
/**
 * m230604_233455_archiveMediaaksesModule_createTable_archives
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2023 OMMU (www.ommu.id)
 * @created date 4 June 2023, 23:35 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\db\Schema;

class m230604_233455_archiveMediaaksesModule_createTable_archives extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archives';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'publish' => Schema::TYPE_TINYINT . '(1) NOT NULL DEFAULT \'1\'',
				'fond_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'archive_name' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'redactor\'',
				'fond_code' => Schema::TYPE_STRING . '(32) NOT NULL',
				'fond_year' => Schema::TYPE_DATE . '(4) NOT NULL',
				'item_define' => Schema::TYPE_INTEGER . '(11) NOT NULL',
				'item_type_id' => Schema::TYPE_SMALLINT . '(5) UNSIGNED',
				'item_unit_id' => Schema::TYPE_SMALLINT . '(5) UNSIGNED',
				'item_count' => Schema::TYPE_INTEGER . '(11) NOT NULL',
				'item_preview' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'file\'',
				'item_file' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'file\'',
				'creation_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger\'',
				'creation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'modified_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger,on_update\'',
				'modified_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'updated_date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT \'0000-00-00 00:00:00\' COMMENT \'trigger\'',
				'_migrate' => Schema::TYPE_TINYINT . '(1) DEFAULT \'0\'',
				'_book_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'PRIMARY KEY ([[id]])',
				'CONSTRAINT ommu_archives_ibfk_1 FOREIGN KEY ([[item_type_id]]) REFERENCES ommu_archive_type ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
				'CONSTRAINT ommu_archives_ibfk_2 FOREIGN KEY ([[item_unit_id]]) REFERENCES ommu_archive_unit ([[id]]) ON DELETE CASCADE ON UPDATE CASCADE',
			], $tableOptions);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archives';
		$this->dropTable($tableName);
	}
}
