<?php
/**
 * m200227_175058_archive_mediaakses_module_insert_role
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 27 February 2020, 17:51 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

class m200227_175058_archive_mediaakses_module_insert_role extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }

        return $authManager;
    }

	public function up()
	{
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;
        $schema = $this->db->getSchema()->defaultSchema;

		$tableName = Yii::$app->db->tablePrefix . $authManager->itemTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['name', 'type', 'description', 'data', 'created_at'], [
				['archiveMAModLevelAdmin', '2', '', 'Archive module for MediaAkses Application user level administartor', time()],
				['archiveMAModLevelModerator', '2', '', 'Archive module for MediaAkses Application user level moderator', time()],
				['archiveMAModLevelMember', '2', 'Archive module for MediaAkses Application user level member', '', time()],
				['/archive/admin/*', '2', '', '', time()],
				['/archive/admin/index', '2', '', '', time()],
				['/archive/book/*', '2', '', '', time()],
				['/archive/book/index', '2', '', '', time()],
				['/archive/view/admin/*', '2', '', '', time()],
				['/archive/view/history/*', '2', '', '', time()],
				['/archive/setting/admin/index', '2', '', time()],
				['/archive/setting/admin/update', '2', '', time()],
				['/archive/setting/admin/delete', '2', '', time()],
				['/archive/setting/type/*', '2', '', '', time()],
				['/archive/setting/type/index', '2', '', '', time()],
				['/archive/setting/unit/*', '2', '', '', time()],
				['/archive/setting/unit/index', '2', '', '', time()],
				['/archive/site/*', '2', '', '', time()],
				['/archive/site/index', '2', '', '', time()],
				['/archive/textual/*', '2', '', '', time()],
				['/archive/textual/index', '2', '', '', time()],
				['/archive/photo/*', '2', '', '', time()],
				['/archive/photo/index', '2', '', '', time()],
				['/archive/cartographic/*', '2', '', '', time()],
				['/archive/cartographic/index', '2', '', '', time()],
				['/fond/*', '2', '', '', time()],
				['/fond/index', '2', '', '', time()],
			]);
		}

		$tableName = Yii::$app->db->tablePrefix . $authManager->itemChildTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['parent', 'child'], [
				['userAdmin', 'archiveMAModLevelAdmin'],
				['userModerator', 'archiveMAModLevelModerator'],
				['archiveMAModLevelAdmin', 'archiveMAModLevelModerator'],
				['archiveMAModLevelAdmin', '/archive/setting/admin/update'],
				['archiveMAModLevelAdmin', '/archive/setting/admin/delete'],
				['archiveMAModLevelModerator', '/archive/setting/admin/index'],
				['archiveMAModLevelModerator', '/archive/setting/type/*'],
				['archiveMAModLevelModerator', '/archive/setting/unit/*'],
				['archiveMAModLevelModerator', '/archive/admin/*'],
				['archiveMAModLevelModerator', '/archive/book/*'],
				['archiveMAModLevelModerator', '/archive/view/admin/*'],
				['archiveMAModLevelModerator', '/archive/view/history/*'],
			]);
		}
	}
}
