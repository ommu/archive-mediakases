<?php
/**
 * m230604_232805_archiveMediaaksesModule_createTable_setting
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2023 OMMU (www.ommu.id)
 * @created date 4 June 2023, 23:31 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\db\Schema;

class m230604_232805_archiveMediaaksesModule_createTable_setting extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_setting';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_TINYINT . '(1) UNSIGNED NOT NULL AUTO_INCREMENT',
				'license' => Schema::TYPE_STRING . '(32) NOT NULL',
				'permission' => Schema::TYPE_TINYINT . '(1) NOT NULL',
				'meta_description' => Schema::TYPE_TEXT . ' NOT NULL',
				'meta_keyword' => Schema::TYPE_TEXT . ' NOT NULL',
				'field' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'item_preview_type' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'item_file_type' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'item_renamed' => Schema::TYPE_TINYINT . '(1) NOT NULL',
				'breadcrumb_param' => Schema::TYPE_TEXT . ' NOT NULL COMMENT \'json\'',
				'modified_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT \'trigger,on_update\'',
				'modified_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'PRIMARY KEY ([[id]])',
			], $tableOptions);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_setting';
		$this->dropTable($tableName);
	}
}
