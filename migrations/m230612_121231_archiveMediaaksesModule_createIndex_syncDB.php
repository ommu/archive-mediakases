<?php
/**
 * m230612_121231_archiveMediaaksesModule_createIndex_syncDB
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2023 OMMU (www.ommu.id)
 * @created date 12 June 2023, 12:13 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use Yii;
use yii\db\Schema;

class m230612_121231_archiveMediaaksesModule_createIndex_syncDB extends \yii\db\Migration
{
	public function up()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archives';
		if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createIndex(
				'_book_id',
				$tableName,
				['_book_id'],
			);
			$this->createIndex(
				'fond_id',
				$tableName,
				['fond_id'],
			);
		}

		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_unit';
		if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createIndex(
				'unit_name',
				$tableName,
				['unit_name'],
			);
		}

		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_type';
		if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createIndex(
				'_id',
				$tableName,
				['_id'],
			);
		}
	}
}
