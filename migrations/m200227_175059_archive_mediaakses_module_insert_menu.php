<?php
/**
 * m200227_175059_archive_mediaakses_module_insert_menu
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 27 February 2020, 17:51 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use app\models\Menu;
use mdm\admin\components\Configs;

class m200227_175059_archive_mediaakses_module_insert_menu extends \yii\db\Migration
{
	public function up()
	{
        $menuTable = Configs::instance()->menuTable;
		$tableName = Yii::$app->db->tablePrefix . $menuTable;
        if (Yii::$app->db->getTableSchema($tableName, true)) {
			$this->batchInsert($tableName, ['name', 'module', 'icon', 'parent', 'route', 'order', 'data'], [
				['MediaAkses', 'archive-mediaakses', null, null, '/#', null, null],
				['Archives', 'archive-mediaakses', null, Menu::getParentId('MediaAkses#archive-mediaakses'), '/archive/admin/index', 1, null],
				['Archive List', 'archive-mediaakses', null, Menu::getParentId('MediaAkses#archive-mediaakses'), '/archive/book/index', 2, null],
				['Settings', 'archive-mediaakses', null, Menu::getParentId('MediaAkses#archive-mediaakses'), '/archive/setting/admin/index', 3, null],
			]);
		}
	}
}
