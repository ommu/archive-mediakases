<?php
/**
 * m230605_013755_archiveMediaaksesModule_addTrigger_all
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2023 OMMU (www.ommu.id)
 * @created date 5 June 2023, 01:38 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\db\Schema;

class m230605_013755_archiveMediaaksesModule_addTrigger_all extends \yii\db\Migration
{
	public function up()
	{
		// alter trigger archiveBeforeUpdate
		$alterTriggerArchiveBeforeUpdate = <<< SQL
CREATE
    TRIGGER `archiveBeforeUpdate` BEFORE UPDATE ON `ommu_archives` 
    FOR EACH ROW BEGIN
	IF (NEW.publish <> OLD.publish) THEN
		SET NEW.updated_date = NOW();
	END IF;
    END;
SQL;
        $this->execute('DROP TRIGGER IF EXISTS `archiveBeforeUpdate`');
		$this->execute($alterTriggerArchiveBeforeUpdate);

		// alter trigger archiveAfterInsertViews
		$alterTriggerArchiveAfterInsertViews = <<< SQL
CREATE
    TRIGGER `archiveAfterInsertViews` AFTER INSERT ON `ommu_archive_views` 
    FOR EACH ROW BEGIN
	IF (NEW.publish = 1 AND NEW.views <> 0) THEN
		INSERT `ommu_archive_view_history` (`view_id`, `view_date`, `view_ip`)
		VALUE (NEW.id, NEW.view_date, NEW.view_ip);
	END IF;
    END;
SQL;
        $this->execute('DROP TRIGGER IF EXISTS `archiveAfterInsertViews`');
		$this->execute($alterTriggerArchiveAfterInsertViews);

		// alter trigger archiveBeforeUpdateViews
		$alterTriggerArchiveBeforeUpdateViews = <<< SQL
CREATE
    TRIGGER `archiveBeforeUpdateViews` BEFORE UPDATE ON `ommu_archive_views` 
    FOR EACH ROW BEGIN
	IF (NEW.publish <> OLD.publish) THEN
		SET NEW.updated_date = NOW();
	ELSE
		IF (NEW.publish = 1 AND (NEW.views <> OLD.views AND NEW.views > OLD.views)) THEN
			SET NEW.view_date = NOW();
		END IF;
	END IF;
    END;
SQL;
        $this->execute('DROP TRIGGER IF EXISTS `archiveBeforeUpdateViews`');
		$this->execute($alterTriggerArchiveBeforeUpdateViews);

		// alter trigger archiveAfterUpdateViews
		$alterTriggerArchiveAfterUpdateViews = <<< SQL
CREATE
    TRIGGER `archiveAfterUpdateViews` AFTER UPDATE ON `ommu_archive_views` 
    FOR EACH ROW BEGIN
	IF (NEW.view_date <> OLD.view_date) THEN
		INSERT `ommu_archive_view_history` (`view_id`, `view_date`, `view_ip`)
		VALUE (NEW.id, NEW.view_date, NEW.view_ip);
	END IF;
    END;
SQL;
        $this->execute('DROP TRIGGER IF EXISTS `archiveAfterUpdateViews`');
		$this->execute($alterTriggerArchiveAfterUpdateViews);
	}

	public function down()
	{
        $this->execute('DROP TRIGGER IF EXISTS `archiveBeforeUpdate`');
        $this->execute('DROP TRIGGER IF EXISTS `archiveAfterInsertViews`');
        $this->execute('DROP TRIGGER IF EXISTS `archiveBeforeUpdateViews`');
        $this->execute('DROP TRIGGER IF EXISTS `archiveAfterUpdateViews`');
	}
}
