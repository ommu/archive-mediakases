<?php
/**
 * archive module config
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 14:39 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

return [
	'id' => 'archive',
	'class' => ommu\archive\Module::className(),
];