<?php
/**
 * Archive Types (archive-type)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\setting\TypeController
 * @var $model ommu\archive\models\ArchiveType
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use app\components\widgets\ActiveForm;
?>

<div class="archive-type-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'type_name')
	->textInput(['maxlength' => true])
	->label($model->getAttributeLabel('type_name')); ?>

<?php echo $form->field($model, 'type_desc')
	->textarea(['rows' => 4, 'cols' => 50])
	->label($model->getAttributeLabel('type_desc')); ?>

<hr/>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>