<?php
/**
 * Archives (archives)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\search\Archives
 * @var $form yii\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use ommu\archive\models\ArchiveType;
use ommu\archive\models\ArchiveUnit;
?>

<div class="archives-search search-form">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'options' => [
			'data-pjax' => 1
		],
	]); ?>

		<?php echo $form->field($model, 'fond_id');?>

		<?php echo $form->field($model, 'archive_name');?>

		<?php echo $form->field($model, 'fond_code');?>

		<?php echo $form->field($model, 'fond_year');?>

		<?php echo $form->field($model, 'item_define');?>

		<?php $itemType = ArchiveType::getType();
		echo $form->field($model, 'item_type_id')
			->dropDownList($itemType, ['prompt' => '']);?>

		<?php $itemUnit = ArchiveUnit::getUnit();
		echo $form->field($model, 'item_unit_id')
			->dropDownList($itemUnit, ['prompt' => '']);?>
		<?php echo $form->field($model, 'item_count');?>

		<?php echo $form->field($model, 'item_preview');?>

		<?php echo $form->field($model, 'item_file');?>

		<?php echo $form->field($model, 'creation_date')
			->input('date');?>

		<?php echo $form->field($model, 'creationDisplayname');?>

		<?php echo $form->field($model, 'modified_date')
			->input('date');?>

		<?php echo $form->field($model, 'modifiedDisplayname');?>

		<?php echo $form->field($model, 'updated_date')
			->input('date');?>

		<?php echo $form->field($model, 'publish')
			->dropDownList($model->filterYesNo(), ['prompt' => '']);?>

		<div class="form-group">
			<?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']); ?>
			<?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']); ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>