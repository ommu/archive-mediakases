<?php
/**
 * Archives (archives)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\Archives
 * @var $searchModel ommu\archive\models\search\Archives
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$context = $this->context;
if ($context->breadcrumbApp) {
	$this->params['breadcrumbs'][] = ['label' => $context->breadcrumbAppParam['name'], 'url' => [$context->breadcrumbAppParam['url']]];
}
if (!$fond) {
    $this->params['breadcrumbs'][] = $this->title;
} else {
	$this->params['breadcrumbs'][] = ['label' => $fond->isArchive ? Yii::t('app', 'Archive') : Yii::t('app', 'Archive List'), 'url' => $fond->isArchive ? ['admin/index'] : ['book/index']];
	$this->params['breadcrumbs'][] = ['label' => $fond->isArchive ? $fond::htmlHardDecode($fond->archive_name) : $fond->fond_code, 'url' => [($fond->isArchive ? 'admin' : 'book').'/view', 'id' => $fond->id]];
	$this->params['breadcrumbs'][] = Yii::t('app', 'Archives');
}

$this->params['menu']['content'] = [
	['label' => Yii::t('app', 'Add {label}', ['label' => !$isArchive ? Yii::t('app', 'List') : Yii::t('app', 'Archive')]), 'url' => Url::to(['create']), 'icon' => 'plus-square', 'htmlOptions' => ['class' => !$isArchive ? 'btn btn-primary modal-btn' : 'btn btn-primary']],
];
$this->params['menu']['option'] = [
	//['label' => Yii::t('app', 'Search'), 'url' => 'javascript:void(0);'],
	['label' => Yii::t('app', 'Grid Option'), 'url' => 'javascript:void(0);'],
];
if ($isArchive) {
    $this->params['menu']['option'] = ArrayHelper::merge($this->params['menu']['option'], [['label' => Yii::t('app', 'Sync Code Access'), 'url' => Url::to(['sync/item/code'])]]);
    $this->params['menu']['option'] = ArrayHelper::merge($this->params['menu']['option'], [['label' => Yii::t('app', 'Export Archive Filename'), 'url' => Url::to(['export'])]]);
    $this->params['menu']['option'] = ArrayHelper::merge($this->params['menu']['option'], [['label' => Yii::t('app', 'Import Archive Filename'), 'url' => Url::to(['import'])]]);
}
?>

<div class="archives-manage">
<?php Pjax::begin(); ?>

<?php if ($type != null) {
    echo $this->render('/setting/type/admin_view', ['model' => $type, 'small' => true]);
} ?>

<?php if ($unit != null) {
    echo $this->render('/setting/unit/admin_view', ['model' => $unit, 'small' => true]);
} ?>

<?php if ($fond != null) {
    echo $this->render('admin_view', ['model' => $fond, 'small' => true]);
} ?>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?php echo $this->render('_option_form', ['model' => $searchModel, 'gridColumns' => $searchModel->activeDefaultColumns($columns), 'route' => $this->context->route]); ?>

<?php
$columnData = $columns;
array_push($columnData, [
	'class' => 'app\components\grid\ActionColumn',
	'header' => Yii::t('app', 'Option'),
	'urlCreator' => function($action, $model, $key, $index) {
        if ($action == 'view') {
            return Url::to(['view', 'id' => $key]);
        }
        if ($action == 'update') {
            return Url::to(['update', 'id' => $key]);
        }
        if ($action == 'delete') {
            return Url::to(['delete', 'id' => $key]);
        }
	},
	'buttons' => [
		'view' => function ($url, $model, $key) {
			// $isArchive = $model->fond_id ? true : false;
			// if (!$isArchive) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Book'), 'class' => 'modal-btn']);
            // }
			// return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'Detail Archive'), 'data-pjax' => 0]);
		},
		'update' => function ($url, $model, $key) {
			$isArchive = $model->fond_id ? true : false;
            if (!$isArchive) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Book'), 'class' => 'modal-btn']);
            }
			return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update Archive'), 'data-pjax' => 0]);
		},
		'delete' => function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
				'title' => Yii::t('app', 'Delete Archive'),
				'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
				'data-method'  => 'post',
			]);
		},
	],
	'template' => '{view} {update} {delete}',
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $columnData,
]); ?>

<?php Pjax::end(); ?>
</div>