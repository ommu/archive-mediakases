<?php
/**
 * Archives (archives)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\Archives
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 6 February 2020, 20:01 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php
$uploadPath = join('/', [$model::getUploadPath(), ($model->isNewFile ? $model->fond_id : 'fileakses')]);
$fileExists = $model->item_file != '' && file_exists(join('/', [$uploadPath, $model->item_file])) ? true : false;

if ($model->item_file && $fileExists) {
	$uploadPath = join('/', [$model::getUploadPath(false), ($model->isNewFile ? $model->fond_id : 'fileakses')]);
	$filePath = Url::to(join('/', ['@webpublic', $uploadPath, $model->item_file]));

	echo \app\components\widgets\PreviewPDF::widget([
		'url' => $filePath,
		'navigationOptions' => ['class' => 'summary mb-4'],
		'previewOptions' => ['class' => 'preview-pdf border border-width-3'],
	]);

} else {?>
	<div class="bs-example" data-example-id="simple-jumbotron">
		<div class="jumbotron">
			<h1><?php echo $model->item_file ? Yii::t('app', 'Archive document not found') : Yii::t('app', 'Archive document not available');?></h1>
			<?php echo $model->item_file ? Html::tag('p', $model->item_file) : '';?>
		</div>
	</div>
<?php }?>