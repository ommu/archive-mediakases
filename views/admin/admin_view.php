<?php
/**
 * Archives (archives)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\Archives
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

if (!$small) {
    $context = $this->context;
    if ($context->breadcrumbApp) {
        $this->params['breadcrumbs'][] = ['label' => $context->breadcrumbAppParam['name'], 'url' => [$context->breadcrumbAppParam['url']]];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->isArchive ? Yii::t('app', 'Archive') : Yii::t('app', 'Archive List'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $model->isArchive ? $model::htmlHardDecode($model->archive_name) : $model->fond_code;
} ?>

<div class="archives-view">
<?php
$attributes = [
	[
		'attribute' => 'id',
		'value' => $model->id ? $model->id : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'publish',
		'value' => $model->quickAction(Url::to(['publish', 'id' => $model->primaryKey]), $model->publish),
		'format' => 'raw',
		'visible' => !$small,
	],
	[
		'attribute' => 'fond_id',
		'value' => function ($model) {
			$fondName = isset($model->fond) ? $model->fond->archive_name : '-';
            if ($fondName != '-') {
                return Html::a($fondName, ['book/view', 'id' => $model->fond_id], ['title' => $fondName, 'class' => 'modal-btn']);
            }
			return $fondName;
		},
		'format' => 'html',
		'visible' => $model->isArchive ? true : false,
	],
	[
		'attribute' => 'fond_code',
		'value' => function ($model) {
            if ($model->isArchive) {
                return $model->fond->fond_code ? $model->fond->fond_code : '-';
            }
            return $model->fond_code ? $model->fond_code : '-';
		},
	],
	[
		'attribute' => 'archive_name',
		'value' => $model->archive_name ? $model->archive_name : '-',
		'format' => 'html',
	],
	[
		'attribute' => 'typeName',
		'value' => function ($model) {
			$typeName = isset($model->type) ? $model->type->type_name : '-';
            if ($typeName != '-') {
                return Html::a($typeName, ['setting/type/view', 'id' => $model->item_type_id], ['title' => $typeName, 'class' => 'modal-btn']);
            }
			return $typeName;
		},
		'format' => 'html',
		'visible' => !$small && $model->isArchive ? true : false,
	],
	[
		'attribute' => 'fond_year',
		'value' => $model->fond_year ? $model->fond_year : '-',
		'visible' => !$model->isArchive ? true : false,
	],
	[
		'attribute' => 'item_define',
		'value' => $model->item_define ? $model->item_define : '-',
		'visible' => !$small && $model->isArchive ? true : false,
	],
	[
		'attribute' => 'item_file',
		'label' => Yii::t('app', 'Code Access'),
		'value' => function ($model) {
            if (!$model->item_file) {
                return '-';
            }

			$uploadPathPreview = join('/', [$model::getUploadPath(false), ($model->isNewFile ? $model->fond_id : 'fileakses')]);

			$itemFile = str_replace('.pdf', '', $model->item_file);

			$uploadPath = join('/', [$model::getUploadPath(), ($model->isNewFile ? $model->fond_id : 'fileakses')]);
			$fileExists = $model->item_file != '' && file_exists(join('/', [$uploadPath, $model->item_file])) ? true : false;

            if ($model->item_file && $fileExists) {
				$itemFile = join('', [$itemFile, '<span class="glyphicon glyphicon-download ml-2"></span>']);
			}

			return $model->item_file ? Html::a($itemFile, Url::to(join('/', ['@webpublic', $uploadPathPreview, $model->item_file])), ['title' => Yii::t('app', 'Download Archive'), 'target' => '_blank']) : '-';
		},
		'format' => 'raw',
		'visible' => !$small && $model->isArchive ? true : false,
	],
	[
		'attribute' => 'item_count',
		'value' => function ($model) {
            if (!$model->item_count) {
                return '-';
            }

            if (isset($model->unit)) {
                return Yii::t('app', '{count} {unit}', ['count' => $model->item_count, 'unit' => $model->unit->unit_name]);
            }

			return $model->item_count;
		},
		'format' => 'html',
		'visible' => !$small && $model->isArchive ? true : false,
	],
	[
		'attribute' => 'subject',
		'value' => $model::parseSubject($model->getSubjects(true, 'title'), 'subjectId'),
		'format' => 'html',
		'visible' => !$small && $model->isArchive && in_array('subject', $model->getEnableField()) ? true : false,
	],
	[
		'attribute' => 'function',
		'value' => $model::parseSubject($model->getFunctions(true, 'title'), 'functionId'),
		'format' => 'html',
		'visible' => !$small && $model->isArchive && in_array('function', $model->getEnableField()) ? true : false,
	],
	[
		'attribute' => 'archives',
		'value' => function ($model) {
			$archives = $model->getArchives(true);
			return $archives ? Html::a($archives, ['admin/manage', 'fond' => $model->primaryKey], ['title' => Yii::t('app', '{count} archives', ['count' => $archives])]) : '-';
		},
		'format' => 'html',
		'visible' => !$small && !$model->isArchive ? true : false,
	],
	[
		'attribute' => 'location',
		'value' => function ($model) {
            if (($location = $model->getLocations(false)) != null) {
                return $model::parseLocation($location);
            }
			return Html::a(Yii::t('app', 'Add archive location'), ['location', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Add archive location'), 'class' => 'modal-btn']);
		},
		'format' => 'html',
		'visible' => !$small ? true : false,
	],
	[
		'attribute' => 'item_preview',
		'value' => function ($model) {
            if (!$model->item_preview) {
                return '-';
            }

			$uploadPath = join('/', [$model::getUploadPath(false), $model->fond_id]);
			return Html::img(Url::to(join('/', ['@webpublic', $uploadPath, $model->item_preview])), ['alt' => $model->item_preview, 'class' => 'd-block border border-width-3 mb-4']).$model->item_preview;
			// return Html::a($model->item_preview, Url::to(join('/', ['@webpublic', $uploadPath, $model->item_preview])), ['class' => 'mb-4']).'<br/>'.$model->item_preview;
		},
		'format' => 'raw',
		'visible' => !$small && $model->isArchive ? true : false,
	],
    [
        'attribute' => 'views',
        'value' => function ($model) {
            $views = $model->getViews(true);
            return Html::a($views, ['view/admin/manage', 'archive' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} views', ['count' => $views]), 'data-pjax' => 0]);
        },
        'format' => 'html',
		'visible' => !$small,
    ],
	[
		'attribute' => 'creation_date',
		'value' => Yii::$app->formatter->asDatetime($model->creation_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'creationDisplayname',
		'value' => isset($model->creation) ? $model->creation->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'modified_date',
		'value' => Yii::$app->formatter->asDatetime($model->modified_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => 'modifiedDisplayname',
		'value' => isset($model->modified) ? $model->modified->displayname : '-',
		'visible' => !$small,
	],
	[
		'attribute' => 'updated_date',
		'value' => Yii::$app->formatter->asDatetime($model->updated_date, 'medium'),
		'visible' => !$small,
	],
	[
		'attribute' => '',
		'value' => function ($model) {
			return Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->primaryKey], ['title' => Yii::t('app', 'Update'), 'class' => Yii::$app->request->isAjax ? (!$model->isArchive ? 'btn btn-primary btn-sm modal-btn' : 'btn btn-primary btn-sm') : 'btn btn-primary btn-sm']);
		},
		'format' => 'html',
		'visible' => !$small && Yii::$app->request->isAjax ? true : false,
	],
];

$archiveInfo = DetailView::widget([
	'model' => $model,
	'options' => [
		'class' => 'table table-striped detail-view',
	],
	'attributes' => $attributes,
]);

echo $this->renderWidget($archiveInfo, [
	'overwrite' => true,
	'cards' => Yii::$app->request->isAjax || $small ? false : true,
]);?>

<?php echo !$small && !Yii::$app->request->isAjax && $model->isArchive ? 
	$this->renderWidget('admin_preview_document', [
		'model' => $model,
	]) : ''; ?>

</div>