<?php
/**
 * Archives (archives)
 * @var $this app\components\View
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\Archives
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;
use yii\redactor\widgets\Redactor;
use ommu\archive\models\ArchiveType;
use ommu\archive\models\ArchiveUnit;
use yii\helpers\ArrayHelper;
use ommu\selectize\Selectize;
use yii\web\JsExpression;
use yii\helpers\Json;

$redactorOptions = [
	'buttons' => ['html', 'format', 'bold', 'italic', 'deleted'],
];
?>

<div class="archives-form">

<?php $form = ActiveForm::begin([
	'options' => [
		'class' => 'form-horizontal form-label-left',
		'enctype' => 'multipart/form-data',
	],
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php $itemType = ArchiveType::getType();
echo $model->isArchive ? $form->field($model, 'item_type_id')
	->widget(Selectize::className(), [
		'options' => [
			'placeholder' => Yii::t('app', 'Select a type..'),
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a type..')], $itemType),
		'pluginOptions' => [
			'valueField' => 'id',
			'labelField' => 'label',
			'searchField' => ['label'],
			'persist' => false,
			'createOnBlur' => false,
			'create' => false,
		],
	])
	->label($model->getAttributeLabel('item_type_id')) : '';?>

<?php if ($model->isArchive) {
	$books = $model::getBooks(1);
	echo $form->field($model, 'fond_id')
		->widget(Selectize::className(), [
			'options' => [
				'placeholder' => Yii::t('app', 'Select a book..'),
			],
			'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a book..')], $books),
			'pluginOptions' => [
				'valueField' => 'id',
				'labelField' => 'label',
				'searchField' => ['label', 'code'],
				'persist' => false,
				'createOnBlur' => false,
				'create' => false,
			],
		])
		->label($model->getAttributeLabel('fond_id'));

	echo $form->field($model, 'archive_name')
		->textarea(['rows' => 6, 'cols' => 50])
		->widget(Redactor::className(), ['clientOptions' => $redactorOptions])
		->label($model->getAttributeLabel('archive_name'));
} else {
	echo $form->field($model, 'archive_name')
		->textInput(['maxlength' => 128])
		->label($model->getAttributeLabel('archive_name'));
} ?>

<?php echo !$model->isArchive ? $form->field($model, 'fond_code')
	->textInput(['maxlength' => true])
	->label($model->getAttributeLabel('fond_code')) : ''; ?>

<?php echo !$model->isArchive ? $form->field($model, 'fond_year')
	->textInput(['type' => 'number', 'maxlength' => true])
	->label($model->getAttributeLabel('fond_year')) : ''; ?>

<?php if ($model->isArchive) {
echo $form->field($model, 'item_define')
	->textInput(['type' => 'number', 'min' => '0'])
	->label($model->getAttributeLabel('item_define'));

$itemUnit = ArchiveUnit::getUnit();
if ($model->isNewRecord && !$model->getErrors()) {
	$model->item_unit_id = 1;
}
if ($model->getErrors()) {
	$item_unit_id = $model->item_unit_id;
    if ($item_unit_id && !ArrayHelper::keyExists($item_unit_id, $itemUnit)) {
        $itemUnit = ArrayHelper::merge([$item_unit_id => $item_unit_id], $itemUnit);
    }
}
$itemUnitId = $form->field($model, 'item_unit_id', ['template' => '{beginWrapper}{input}{endWrapper}', 'horizontalCssClasses' => ['wrapper' => 'col-md-3 col-sm-5 col-xs-6'], 'options' => ['tag' => null]])
	->widget(Selectize::className(), [
		'options' => [
			'placeholder' => Yii::t('app', 'Select a unit..'),
		],
		'items' => ArrayHelper::merge(['' => Yii::t('app', 'Select a unit..')], $itemUnit),
		'pluginOptions' => [
			'valueField' => 'id',
			'labelField' => 'label',
			'searchField' => ['label'],
			'persist' => false,
			'createOnBlur' => false,
			'create' => true,
		],
	])
	->label($model->getAttributeLabel('item_unit_id'));?>

<?php echo $form->field($model, 'item_count', ['template' => '{label}{beginWrapper}{input}{endWrapper}'.$itemUnitId.'{error}', 'horizontalCssClasses' => ['wrapper' => 'col-md-3 col-sm-4 col-xs-6', 'error' => 'col-md-6 col-sm-9 col-xs-12 col-sm-offset-3']])
	->textInput(['type' => 'number', 'min' => '0'])
	->label($model->getAttributeLabel('item_count')); ?>

<hr/>

<?php if ($model->isArchive) {
$subjectSuggestUrl = Url::to(['/admin/tag/suggest']);
echo in_array('subject', $setting->field) ? $form->field($model, 'subject')
	->widget(Selectize::className(), [
		'url' => $subjectSuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'label',
			'labelField' => 'label',
			'searchField' => ['label'],
			'persist' => false,
			'createOnBlur' => false,
			'create' => true,
		],
	])
	->label($model->getAttributeLabel('subject')) : '';?>

<?php echo in_array('function', $setting->field) ? $form->field($model, 'function')
	->widget(Selectize::className(), [
		'url' => $subjectSuggestUrl,
		'queryParam' => 'term',
		'pluginOptions' => [
			'valueField' => 'label',
			'labelField' => 'label',
			'searchField' => ['label'],
			'persist' => false,
			'createOnBlur' => false,
			'create' => true,
		],
	])
	->label($model->getAttributeLabel('function')): '';?>

<?php echo in_array('subject', $setting->field) || in_array('function', $setting->field) ? '<hr/>' : '';
}?>

<?php $uploadPath = join('/', [$model::getUploadPath(false), $model->fond_id]);
	$itemFile = !$model->isNewRecord && $model->old_item_preview != '' ? Html::img(Url::to(join('/', ['@webpublic', $uploadPath, $model->old_item_preview])), ['title' => $model->old_item_preview, 'class' => 'd-block border border-width-3 mb-4']).$model->old_item_preview.'<hr/>' : '';
	echo $form->field($model, 'item_preview', ['template' => '{label}{beginWrapper}<div>'.$itemFile.'</div>{input}{error}{hint}{endWrapper}'])
		->fileInput()
		->label($model->getAttributeLabel('item_preview'))
		->hint(Yii::t('app', 'extensions are allowed: {extensions}', ['extensions' => $setting->item_preview_type])); ?>

<hr/>

<?php $uploadPath = join('/', [$model::getUploadPath(false), $model->fond_id]);
	$itemFile = !$model->isNewRecord && $model->old_item_file != '' ? Html::a($model->old_item_file, Url::to(['preview', 'id' => $model->id]), ['title' => $model->old_item_file, 'class' => 'd-block mb-4 modal-btn']) : '';
	echo $form->field($model, 'item_file', ['template' => '{label}{beginWrapper}<div>'.$itemFile.'</div>{input}{error}{hint}{endWrapper}'])
		->fileInput()
		->label($model->getAttributeLabel('item_file'))
		->hint(Yii::t('app', 'extensions are allowed: {extensions}', ['extensions' => $setting->item_file_type]));
} ?>

<hr/>

<?php 
if ($model->isNewRecord && !$model->getErrors()) {
    $model->publish = 1;
}
echo $form->field($model, 'publish')
	->checkbox()
	->label($model->getAttributeLabel('publish')); ?>

<hr/>

<?php 
if ($model->isArchive) {
    $model->backToManage = 1;
    echo $form->field($model, 'backToManage')
        ->checkbox()
        ->label($model->getAttributeLabel('backToManage')); ?>

    <hr/>
<?php }?>

<?php echo $form->field($model, 'submitButton')
	->submitButton(); ?>

<?php ActiveForm::end(); ?>

</div>