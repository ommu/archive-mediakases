<?php
/**
 * AdminController
 * @var $this ommu\archive\controllers\AdminController
 * @var $model ommu\archive\models\Archives
 *
 * AdminController implements the CRUD actions for Archives model.
 * Reference start
 * TOC :
 *	Index
 *	Manage
 *	Create
 *	Update
 *	View
 *	Delete
 *	RunAction
 *	Publish
 *	Location
 *	ResetLocation
 *	Preview
 *	Import
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\controllers;

use Yii;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use yii\filters\VerbFilter;
use ommu\archive\models\Archives;
use ommu\archive\models\search\Archives as ArchivesSearch;
use ommu\archive\models\ArchiveSetting;
use yii\web\UploadedFile;
use ommu\archiveLocation\models\ArchiveLocations;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use thamtech\uuid\helpers\UuidHelper;

class AdminController extends Controller
{
	use \ommu\traits\FileTrait;

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
        parent::init();

        if (Yii::$app->request->get('id')) {
            if ($this->isArchive() == true) {
                $this->subMenu = $this->module->params['archive_submenu'];
            } else {
                $this->subMenu = $this->module->params['fond_submenu'];
            }
        }

		$setting = ArchiveSetting::find()
			->select(['breadcrumb_param'])
			->where(['id' => 1])
			->one();
		$this->breadcrumbApp = $setting->breadcrumb;
		$this->breadcrumbAppParam = $setting->getBreadcrumbAppParam();
	}

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish' => ['POST'],
                ],
            ],
        ];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex()
	{
        return $this->redirect(['manage']);
	}

	/**
	 * Lists all Archives models.
	 * @return mixed
	 */
	public function actionManage()
	{
        $searchModel = new ArchivesSearch(['isArchive' => $this->isArchive()]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $gridColumn = Yii::$app->request->get('GridColumn', null);
        $cols = [];
        if ($gridColumn != null && count($gridColumn) > 0) {
            foreach ($gridColumn as $key => $val) {
                if ($gridColumn[$key] == 1) {
                    $cols[] = $key;
                }
            }
        }
        $columns = $searchModel->getGridColumn($cols);

        if (($type = Yii::$app->request->get('type')) != null) {
            $type = \ommu\archive\models\ArchiveType::findOne($type);
        }
        if (($unit = Yii::$app->request->get('unit')) != null) {
            $unit = \ommu\archive\models\ArchiveUnit::findOne($unit);
        }
        if (($fond = Yii::$app->request->get('fond')) != null) {
            $fond = \ommu\archive\models\Archives::findOne($fond);
			$fond->isArchive = false;
			$attributes = [
				'archive_name' => Yii::t('app', 'Title')
			];
			$fond->setAttributeLabels($attributes);
		}

		$this->view->title = !$this->isArchive() ? Yii::t('app', 'Archive Lists') : Yii::t('app', 'Archives');
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->render('admin_manage', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'columns' => $columns,
			'type' => $type,
			'unit' => $unit,
			'fond' => $fond,
			'isArchive' => $this->isArchive(),
		]);
	}

	/**
	 * Creates a new Archives model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
        $model = new Archives(['isArchive' => $this->isArchive()]);
        if (!$model->isArchive) {
			$attributes = [
				'archive_name' => Yii::t('app', 'Title')
			];
			$model->setAttributeLabels($attributes);
		}
		$setting = ArchiveSetting::find()
			->select(['field', 'item_preview_type', 'item_file_type'])
			->where(['id' => 1])
            ->one();

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->item_define = $postData['item_define'] ? $postData['item_define'] : 0;
            $model->item_preview = UploadedFile::getInstance($model, 'item_preview');
            if (!($model->item_preview instanceof UploadedFile && !$model->item_preview->getHasError())) {
                $model->item_preview = $postData['item_preview'] ? $postData['item_preview'] : '';
            }
			$model->item_file = UploadedFile::getInstance($model, 'item_file');
            if (!($model->item_file instanceof UploadedFile && !$model->item_file->getHasError())) {
                $model->item_file = $postData['item_file'] ? $postData['item_file'] : '';
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', '{label} success created.', ['label' => !$model->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));

                if (!$model->backToManage) {
                    if (!Yii::$app->request->isAjax) {
						return $this->redirect(['create']);
                    }
					return $this->redirect(Yii::$app->request->referrer ?: ['create']);
				}
                if (!Yii::$app->request->isAjax) {
                    return $this->redirect(['manage']);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

		$this->view->title = Yii::t('app', 'Create {label}', ['label' => !$model->isArchive ? Yii::t('app', 'Archive List') : Yii::t('app', 'Archive')]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_create', [
			'model' => $model,
			'setting' => $setting,
		]);
	}

	/**
	 * Updates an existing Archives model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$setting = ArchiveSetting::find()
			->select(['field', 'item_preview_type', 'item_file_type'])
			->where(['id' => 1])
            ->one();

        if (Yii::$app->request->isPost) {
            $postData = Yii::$app->request->post();
            $model->load($postData);
            $model->item_define = $postData['item_define'] ? $postData['item_define'] : 0;
            $model->item_preview = UploadedFile::getInstance($model, 'item_preview');
            $model->item_file = UploadedFile::getInstance($model, 'item_file');

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', '{label} success updated.', ['label' => !$model->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));

                if (!$model->backToManage) {
                    if (!Yii::$app->request->isAjax) {
						return $this->redirect(['update', 'id' => $model->id]);
                    }
					return $this->redirect(Yii::$app->request->referrer ?: ['update', 'id' => $model->id]);
				}
                if (!Yii::$app->request->isAjax) {
                    return $this->redirect(['manage']);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['manage']);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

        $this->subMenuParam = $model->id;
		$this->view->title = Yii::t('app', 'Update {label}: {archive-name}', ['label' => !$model->isArchive ? Yii::t('app', 'Archive List') : Yii::t('app', 'Archive'), 'archive-name' => $model::htmlHardDecode($model->archive_name)]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_update', [
			'model' => $model,
			'setting' => $setting,
		]);
	}

	/**
	 * Displays a single Archives model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
        $model = $this->findModel($id);

        $this->subMenuParam = $model->id;
		$this->view->cards = false;
		$this->view->title = Yii::t('app', 'Detail {label}: {archive-name}', ['label' => !$model->isArchive ? Yii::t('app', 'Archive List') : Yii::t('app', 'Archive'), 'archive-name' => $model::htmlHardDecode($model->archive_name)]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_view', [
			'model' => $model,
			'small' => false,
		]);
	}

	/**
	 * Deletes an existing Archives model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->publish = 2;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', '{label} success deleted.', ['label' => !$model->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * actionPublish an existing Archives model.
	 * If publish is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPublish($id)
	{
		$model = $this->findModel($id);
		$replace = $model->publish == 1 ? 0 : 1;
		$model->publish = $replace;

        if ($model->save(false, ['publish', 'modified_id'])) {
			Yii::$app->session->setFlash('success', Yii::t('app', '{label} success updated.', ['label' => !$model->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));
			return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
		}
	}

	/**
	 * Finds the Archives model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Archives the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
        if (($model = Archives::findOne($id)) !== null) {
			$model->isArchive = $this->isArchive();
            if (!$model->isArchive) {
				$attributes = [
					'archive_name' => Yii::t('app', 'Title')
				];
				$model->setAttributeLabels($attributes);
			}

			return $model;
		}

		throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function isArchive()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getViewPath()
	{
		return $this->module->getViewPath() . DIRECTORY_SEPARATOR . 'admin';
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionLocation($id)
	{
		$model = ArchiveLocations::find()
			->where(['archive_id' => $id])
			->one();
		$newRecord = false;
        if ($model == null) {
			$newRecord = true;
			$model = new ArchiveLocations(['archive_id' => $id]);
		}
		$model->archive->isArchive = $this->isArchive();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            // $postData = Yii::$app->request->post();
            // $model->load($postData);
            // $model->order = $postData['order'] ? $postData['order'] : 0;

            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', '{label} success updated location.', ['label' => !$model->archive->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));
                if (!Yii::$app->request->isAjax) {
					return $this->redirect(['location', 'id' => $model->archive_id]);
                }
                return $this->redirect(Yii::$app->request->referrer ?: ['location', 'id' => $model->archive_id]);

            } else {
                if (Yii::$app->request->isAjax) {
                    return \yii\helpers\Json::encode(\app\components\widgets\ActiveForm::validate($model));
                }
            }
        }

        $this->subMenuParam = $model->archive_id;
		$this->view->title = Yii::t('app', 'Location {label}: {archive-name}', ['label' => !$model->archive->isArchive ? Yii::t('app', 'Archive List') : Yii::t('app', 'Archive'), 'archive-name' => $model::htmlHardDecode($model->archive->archive_name)]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_location', [
			'model' => $model,
			'newRecord' => $newRecord,
		]);
	}

	/**
	 * Deletes an existing Archives model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionResetLocation($id)
	{
        if (($model = ArchiveLocations::find()->where(['id' => $id])->one()) === null) {
			throw new \yii\web\NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

		$model->archive->isArchive = $this->isArchive();
		$model->delete();

		Yii::$app->session->setFlash('success', Yii::t('app', '{label} success reset location.', ['label' => !$model->archive->isArchive ? Yii::t('app', 'Archive list') : Yii::t('app', 'Archive')]));
		return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
	}

	/**
	 * Displays a single Archives model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionPreview($id)
	{
		$model = $this->findModel($id);

		$this->view->title = Yii::t('app', 'Preview {label}: {archive-name}', ['label' => !$model->isArchive ? Yii::t('app', 'Archive List') : Yii::t('app', 'Archive'), 'archive-name' => $model::htmlHardDecode($model->archive_name)]);
		$this->view->description = '';
		$this->view->keywords = '';
		return $this->oRender('admin_preview_document', [
			'model' => $model,
		]);
	}

	/**
	 * Displays a single Archives model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionExport()
	{
        $model = Archives::find()
            ->select(['id', 'archive_name', 'fond_code', 'item_define'])
            ->andWhere(['is not', 'fond_id', null])
            ->andWhere(['=', 'item_file', ''])
            ->all();

        if ($model) {
            $spreadsheet = new Spreadsheet();

            // Set document properties
            $spreadsheet->getProperties()->setCreator('Putra Sudaryanto')
                ->setLastModifiedBy('Putra Sudaryanto')
                ->setTitle('Office 2007 XLSX Update Archive Filename')
                ->setSubject('Office 2007 XLSX Update Archive Filename')
                ->setDescription('Update Archive Filename Document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Update archive filename');
    
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Archive ID')
                ->setCellValue('B1', 'Archive Description')
                ->setCellValue('C1', 'Code Access')
                ->setCellValue('D1', 'Definitive Number')
                ->setCellValue('E1', 'Archive Filename');

            $i = 2;
            foreach ($model as $val) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $val->id)
                    ->setCellValue('B' . $i, $val->archive_name)
                    ->setCellValue('C' . $i, $val->fond_code)
                    ->setCellValue('D' . $i, $val->item_define);
                $i++;
            }

            $spreadsheet->getActiveSheet()->setTitle('Company Industry');
    
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);
    
            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="01simple.xlsx"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');
    
            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0
    
            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');  
            Yii::$app->end();

        } else {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Archive filename complete.'));
            return $this->redirect(Yii::$app->request->referrer ?: ['manage']);
        }

        // $baseUrl = Yii::getAlias('@web');
        // return $this->redirect(join('/', [$baseUrl, 'archive/_import/archiveFilename_update.xlsx']));

        // $total = 0;

	}

	/**
	 * Import a new ArchivePengolahanPenyerahanItem model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionImport()
	{
        if (Yii::$app->request->isPost) {
			$archivePath = Archives::getUploadPath();
			$archiveImportPath = join('/', [$archivePath, '_import']);
			$verwijderenPath = join('/', [$archiveImportPath, 'verwijderen']);
			$this->createUploadDirectory($archiveImportPath);

			$errors = [];
			$importFilename = UploadedFile::getInstanceByName('importFilename');
            if ($importFilename instanceof UploadedFile && !$importFilename->getHasError()) {
				$importFileType = ['xlsx', 'xls'];
                if (in_array(strtolower($importFilename->getExtension()), $importFileType)) {
					$fileName = join('_', ['archiveFilename', time(), 'import', UuidHelper::uuid()]);
					$fileNameExtension = $fileName.'.'.strtolower($importFilename->getExtension());

                    if ($importFilename->saveAs(join('/', [$archiveImportPath, $fileNameExtension]))) {
						$spreadsheet = IOFactory::load(join('/', [$archiveImportPath, $fileNameExtension]));
						$sheetData = $spreadsheet->getActiveSheet()->toArray();

						try {
							foreach ($sheetData as $key => $value) {
                                if ($key == 0) {
                                    continue;
                                }

                                $id             = trim($value[0]);
								$item_file      = trim($value[4]);

                                if (($model = Archives::findOne($id)) === null) {
                                    continue;
                                }

								$model->item_file = $item_file;
                                $model->update(false);
							}
							Yii::$app->session->setFlash('success', Yii::t('app', 'Update archive filename success imported.'));
						} catch (\Exception $e) {
							throw $e;
						} catch (\Throwable $e) {
							throw $e;
						}
					}
	
				} else {
					Yii::$app->session->setFlash('error', Yii::t('app', 'The file {name} cannot be uploaded. Only files with these extensions are allowed: {extensions}', [
						'name' => $importFilename->name,
						'extensions' => $importFileType,
					]));
				}
			} else {
				Yii::$app->session->setFlash('error', Yii::t('app', 'Import file cannot be blank.'));
            }

            if (!Yii::$app->request->isAjax) {
				return $this->redirect(['import']);
            }
			return $this->redirect(Yii::$app->request->referrer ?: ['import']);
		}

		$this->view->title = Yii::t('app', 'Import Archive Filename');
		$this->view->description = '';
        if (Yii::$app->request->isAjax) {
			$this->view->description = Yii::t('app', 'Are you sure you want to import update archive filename?');
        }
		$this->view->keywords = '';
		return $this->oRender('admin_import');
	}
}
