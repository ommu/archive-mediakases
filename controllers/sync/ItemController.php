<?php
/**
 * ItemController
 * @var $this app\components\View
 *
 * Reference start
 * TOC :
 *  Code
 *  Index
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2023 OMMU (www.ommu.id)
 * @created date 8 June 2023, 02:39 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\controllers\sync;

use Yii;
use app\components\Controller;
use mdm\admin\components\AccessControl;
use ommu\archive\models\Archives;

class ItemController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function allowAction(): array {
		return ['index', 'code'];
	}

	/**
	 * Index Action
	 */
	public function actionIndex()
	{
        return $this->redirect(['admin/manage']);
	}

	/**
	 * Code Action
	 */
	public function actionCode($total=0)
	{
        $model = Archives::find()
            ->select(['id', 'fond_code', 'item_file'])
            ->andWhere(['is not', 'fond_id', null])
            ->andWhere(['=', 'fond_code', ''])
            ->andWhere(['<>', 'item_file', ''])
			->limit(1000)
            ->all();

        if ($model) {
            foreach ($model as $val) {
                $total ++;
                $val->fond_code = str_replace('.PDF', '', strtoupper($val->item_file));
                if (!$val->update(false)) {
                    continue;
                }
            }
            return $this->redirect(Yii::$app->request->referrer ?: ['code', 'total' => $total]);

        } else {
            Yii::$app->session->setFlash('success', Yii::t('app', '{total} success sync.', ['total' => $total]));
            return $this->redirect(Yii::$app->request->referrer ?: ['admin/manage']);
        }
	}

}
