<?php
/**
 * BookController
 * @var $this ommu\archive\controllers\BookController
 * @var $model ommu\archive\models\Archives
 *
 * BookController implements the CRUD actions for Archives model.
 * Reference start
 * TOC :
 *	@see \ommu\archive\controllers\AdminController
 *
 *	findModel
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\controllers;

use Yii;

class BookController extends AdminController
{
	/**
	 * {@inheritdoc}
	 */
	public function isArchive()
	{
		return false;
	}
}
