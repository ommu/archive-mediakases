<?php
/**
 * ArchiveUnit
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:01 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 * This is the model class for table "ommu_archive_unit".
 *
 * The followings are the available columns in table "ommu_archive_unit":
 * @property integer $id
 * @property string $unit_name
 * @property string $unit_desc
 * @property string $creation_date
 * @property integer $creation_id
 *
 * The followings are the available model relations:
 * @property Archives[] $archives
 * @property Users $creation
 *
 */

namespace ommu\archive\models;

use Yii;
use yii\helpers\Html;
use app\models\Users;

class ArchiveUnit extends \app\components\ActiveRecord
{
	public $gridForbiddenColumn = ['unit_desc', 'creation_date', 'creationDisplayname'];

	public $creationDisplayname;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_archive_unit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['unit_name'], 'required'],
			[['creation_id'], 'integer'],
			[['unit_desc'], 'string'],
			[['unit_desc'], 'safe'],
			[['unit_name'], 'string', 'max' => 32],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'unit_name' => Yii::t('app', 'Unit'),
			'unit_desc' => Yii::t('app', 'Description'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'archives' => Yii::t('app', 'Archives'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getArchives($count=false, $publish=1)
	{
        if ($count == false) {
            return $this->hasMany(Archives::className(), ['item_unit_id' => 'id'])
                ->alias('archives')
                ->andOnCondition([sprintf('%s.publish', 'archives') => $publish]);
        }

		$model = Archives::find()
            ->alias('t')
            ->where(['item_unit_id' => $this->id]);
        if ($publish == 0) {
            $model->unpublish();
        } else if ($publish == 1) {
            $model->published();
        } else if ($publish == 2) {
            $model->deleted();
        }
		$archives = $model->count();

		return $archives ? $archives : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\query\ArchiveUnit the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\archive\models\query\ArchiveUnit(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['unit_name'] = [
			'attribute' => 'unit_name',
			'value' => function($model, $key, $index, $column) {
				return $model->unit_name;
			},
		];
		$this->templateColumns['unit_desc'] = [
			'attribute' => 'unit_desc',
			'value' => function($model, $key, $index, $column) {
				return $model->unit_desc;
			},
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
		$this->templateColumns['archives'] = [
			'attribute' => 'archives',
			'value' => function($model, $key, $index, $column) {
				$archives = $model->getArchives(true);
				return Html::a($archives, ['admin/manage', 'unit' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} archives', ['count' => $archives]), 'data-pjax' => 0]);
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * function getUnit
	 */
	public static function getUnit($array=true)
	{
		$model = self::find()
            ->alias('t')
			->select(['t.id', 't.unit_name']);
		$model = $model->orderBy('t.unit_name ASC')->all();

        if ($array == true) {
            return \yii\helpers\ArrayHelper::map($model, 'id', 'unit_name');
        }

		return $model;
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}
}
