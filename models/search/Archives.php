<?php
/**
 * Archives
 *
 * Archives represents the model behind the search form about `ommu\archive\models\Archives`.
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:06 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ommu\archive\models\Archives as ArchivesModel;

class Archives extends ArchivesModel
{
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['id', 'publish', 'fond_id', 'item_define', 'item_type_id', 'item_unit_id', 'item_count', 'creation_id', 'modified_id', 
                'preview', 'file', 'location',
                'subjectId', 'functionId',
                'rackId', 'roomId', 'depoId', 'buildingId'], 'integer'],
			[['archive_name', 'fond_code', 'fond_year', 'item_preview', 'item_file', 'creation_date', 'modified_date', 'updated_date', 'fondName', 'typeName', 'unitName', 'creationDisplayname', 'modifiedDisplayname'], 'safe'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Tambahkan fungsi beforeValidate ini pada model search untuk menumpuk validasi pd model induk. 
	 * dan "jangan" tambahkan parent::beforeValidate, cukup "return true" saja.
	 * maka validasi yg akan dipakai hanya pd model ini, semua script yg ditaruh di beforeValidate pada model induk
	 * tidak akan dijalankan.
	 */
	public function beforeValidate() {
		return true;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $column=null)
	{
        if (!($column && is_array($column))) {
            $query = ArchivesModel::find()->alias('t');
        } else {
            $query = ArchivesModel::find()->alias('t')->select($column);
        }
		$query->joinWith([
			// 'type type', 
			// 'unit unit', 
			// 'creation creation', 
			// 'modified modified'
		]);
        if ((isset($params['sort']) && in_array($params['sort'], ['fondName', '-fondName'])) || (isset($params['fondName']) && $params['fondName'] != '')) {
            $query->joinWith(['fond fond']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['item_type_id', '-item_type_id'])) || (isset($params['item_type_id']) && $params['item_type_id'] != '')) {
            $query->joinWith(['type type']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['item_unit_id', '-item_unit_id'])) || (isset($params['item_unit_id']) && $params['item_unit_id'] != '')) {
            $query->joinWith(['unit unit']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['creationDisplayname', '-creationDisplayname'])) || (isset($params['creationDisplayname']) && $params['creationDisplayname'] != '')) {
            $query->joinWith(['creation creation']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['modifiedDisplayname', '-modifiedDisplayname'])) || (isset($params['modifiedDisplayname']) && $params['modifiedDisplayname'] != '')) {
            $query->joinWith(['modified modified']);
        }
        if (isset($params['subjectId']) && $params['subjectId'] != '') {
            $query->joinWith(['subjects subjects']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['subject', '-subject'])) || 
            (isset($params['subject']) && $params['subject'] != '')
        ) {
            $query->joinWith(['subjects.tag subject']);
        }
        if (isset($params['functionId']) && $params['functionId'] != '') {
            $query->joinWith(['functions functions']);
        }
        if ((isset($params['sort']) && in_array($params['sort'], ['function', '-function'])) || 
            (isset($params['function']) && $params['function'] != '')
        ) {
            $query->joinWith(['functions.tag function']);
        }

        // location
        if ((isset($params['location']) && $params['location'] != '') || 
            (isset($params['rackId']) && $params['rackId'] != '') || 
            (isset($params['roomId']) && $params['roomId'] != '') || 
            (isset($params['depoId']) && $params['depoId'] != '') || 
            (isset($params['buildingId']) && $params['buildingId'] != '')
        ) {
            $query->joinWith(['locations locations']);
        }
        if (isset($params['depoId']) && $params['depoId'] != '') {
            $query->joinWith(['locations.room relatedLocationRoom']);
        }
        if (isset($params['buildingId']) && $params['buildingId'] != '') {
            $query->joinWith(['locations.room.parent relatedLocationDepo']);
        }

		$query->groupBy(['id']);

        // add conditions that should always apply here
		$dataParams = [
			'query' => $query,
		];
        // disable pagination agar data pada api tampil semua
        if (isset($params['pagination']) && $params['pagination'] == 0) {
            $dataParams['pagination'] = false;
        }
		$dataProvider = new ActiveDataProvider($dataParams);

		$attributes = array_keys($this->getTableSchema()->columns);
		$attributes['fondName'] = [
			'asc' => ['fond.archive_name' => SORT_ASC],
			'desc' => ['fond.archive_name' => SORT_DESC],
		];
		$attributes['item_type_id'] = [
			'asc' => ['type.type_name' => SORT_ASC],
			'desc' => ['type.type_name' => SORT_DESC],
		];
		$attributes['item_unit_id'] = [
			'asc' => ['unit.unit_name' => SORT_ASC],
			'desc' => ['unit.unit_name' => SORT_DESC],
		];
		$attributes['creationDisplayname'] = [
			'asc' => ['creation.displayname' => SORT_ASC],
			'desc' => ['creation.displayname' => SORT_DESC],
		];
		$attributes['modifiedDisplayname'] = [
			'asc' => ['modified.displayname' => SORT_ASC],
			'desc' => ['modified.displayname' => SORT_DESC],
		];
		$dataProvider->setSort([
			'attributes' => $attributes,
			'defaultOrder' => ['id' => SORT_DESC],
		]);

        if (Yii::$app->request->get('id')) {
            unset($params['id']);
        }
		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		// grid filtering conditions
		$query->andFilterWhere([
			't.id' => $this->id,
			't.fond_id' => isset($params['fond']) ? $params['fond'] : $this->fond_id,
			'cast(t.fond_year as date)' => $this->fond_year,
			't.item_define' => $this->item_define,
			't.item_type_id' => isset($params['type']) ? $params['type'] : $this->item_type_id,
			't.item_unit_id' => isset($params['unit']) ? $params['unit'] : $this->item_unit_id,
			't.item_count' => $this->item_count,
			'cast(t.creation_date as date)' => $this->creation_date,
			't.creation_id' => isset($params['creation']) ? $params['creation'] : $this->creation_id,
			'cast(t.modified_date as date)' => $this->modified_date,
			't.modified_id' => isset($params['modified']) ? $params['modified'] : $this->modified_id,
			'cast(t.updated_date as date)' => $this->updated_date,
		]);

        if (isset($params['trash'])) {
            $query->andFilterWhere(['NOT IN', 't.publish', [0,1]]);
        } else {
            if (!isset($params['publish']) || (isset($params['publish']) && $params['publish'] == '')) {
                $query->andFilterWhere(['IN', 't.publish', [0,1]]);
            } else {
                $query->andFilterWhere(['t.publish' => $this->publish]);
            }
        }

		// archive / fond
        if ($this->isArchive) {
            $query->andWhere(['is not', 't.fond_id', null]);
        } else {
            $query->andWhere(['is', 't.fond_id', null]);
        }

        if (isset($params['fondName']) && $params['fondName'] != '') {
			$query->andWhere(['or', 
                ['like', 'fond.archive_name', $this->fondName],
                ['like', 'fond.fond_code', $this->fondName]
            ]);
		}

		$query->andFilterWhere(['subjects.tag_id' => $this->subjectId]);
		$query->andFilterWhere(['functions.tag_id' => $this->functionId]);

        // location
		$query->andFilterWhere(['locations.rack_id' => $this->rackId]);
		$query->andFilterWhere(['locations.room_id' => $this->roomId]);
		$query->andFilterWhere(['relatedLocationRoom.parent_id' => $this->depoId]);
		$query->andFilterWhere(['relatedLocationDepo.parent_id' => $this->buildingId]);

		// location (yes/no)
        if (isset($params['location']) && $params['location'] != '') {
            if ($this->location == 1) {
                $query->andWhere(['is not', 'locations.id', null]);
            } else if ($this->location == 0) {
                $query->andWhere(['is', 'locations.id', null]);
            }
        }

		// preview (yes/no)
        if (isset($params['preview']) && $params['preview'] != '') {
            if ($this->preview == 1) {
                $query->andWhere(['<>', 't.item_preview', '']);
            } else if ($this->preview == 0) {
                $query->andWhere(['=', 't.item_preview', '']);
            }
        }

		// file (yes/no)
        if (isset($params['file']) && $params['file'] != '') {
            if ($this->file == 1) {
                $query->andWhere(['<>', 't.item_file', '']);
            } else if ($this->file == 0) {
                $query->andWhere(['=', 't.item_file', '']);
            }
        }

		$query->andFilterWhere(['like', 't.archive_name', $this->archive_name])
			->andFilterWhere(['like', 't.fond_code', $this->fond_code])
			->andFilterWhere(['like', 't.item_preview', $this->item_preview])
			->andFilterWhere(['like', 't.item_file', $this->item_file])
			->andFilterWhere(['like', 'creation.displayname', $this->creationDisplayname])
			->andFilterWhere(['like', 'modified.displayname', $this->modifiedDisplayname])

			->andFilterWhere(['like', 'type.type_name', $this->typeName])
			->andFilterWhere(['like', 'unit.unit_name', $this->unitName])
			->andFilterWhere(['like', 'subjectsRltn.body', $this->subject])
			->andFilterWhere(['like', 'functionsRltn.body', $this->function]);

		return $dataProvider;
	}
}
