<?php
/**
 * Archives
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:02 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 * This is the model class for table "ommu_archives".
 *
 * The followings are the available columns in table "ommu_archives":
 * @property integer $id
 * @property integer $publish
 * @property integer $fond_id
 * @property string $archive_name
 * @property string $fond_code
 * @property string $fond_year
 * @property integer $item_define
 * @property integer $item_type_id
 * @property integer $item_unit_id
 * @property integer $item_count
 * @property string $item_preview
 * @property string $item_file
 * @property string $creation_date
 * @property integer $creation_id
 * @property string $modified_date
 * @property integer $modified_id
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property ArchiveLocations[] $locations
 * @property ArchiveSubject[] $subjects
 * @property ArchiveSubject[] $functions
 * @property ArchiveType $type
 * @property ArchiveUnit $unit
 * @property Users $creation
 * @property Users $modified
 * @property Archives[] $archives
 * @property ArchiveViews[] $views
 *
 */

namespace ommu\archive\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;
use thamtech\uuid\helpers\UuidHelper;
use app\models\Users;
use ommu\archiveLocation\models\ArchiveLocations;
use app\models\CoreTags;
use yii\helpers\Inflector;

class Archives extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;
	use \ommu\traits\FileTrait;

	public $gridForbiddenColumn = ['item_unit_id', 'item_preview', 'creation_date', 'creationDisplayname', 'modified_date', 'modifiedDisplayname', 'updated_date', 'location', 'subject', 'function'];

	public $old_item_preview;
	public $old_item_file;
	public $isArchive = true;
	public $preview;
	public $file;
	public $location;
	public $subject;
	public $function;
	public $backToManage;

	public $fondName;
	public $creationDisplayname;
	public $modifiedDisplayname;
	public $typeName;
	public $unitName;

    public $subjectId;
    public $functionId;

    public $rackId;
    public $roomId;
    public $depoId;
    public $buildingId;

	const SCENARIO_FOND = 'isFond';
	const SCENARIO_ITEM = 'isArchiveItem';

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_archives';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['publish', 'archive_name'], 'required'],
			[['fond_code', 'fond_year'], 'required', 'on' => self::SCENARIO_FOND],
			[['fond_id', 'item_type_id', 'item_unit_id', 'item_count'], 'required', 'on' => self::SCENARIO_ITEM],
			[['publish', 'fond_id', 'item_define', 'item_type_id', 'item_count', 'creation_id', 'modified_id', 'backToManage'], 'integer'],
			[['archive_name', 'item_preview', 'item_file'], 'string'],
			[['fond_year', 'item_define', 'item_unit_id', 'item_preview', 'item_file', 'subject', 'function', 'backToManage'], 'safe'],
			[['fond_code'], 'string', 'max' => 64],
			[['fond_year'], 'string', 'max' => 4],
			[['item_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArchiveType::className(), 'targetAttribute' => ['item_type_id' => 'id']],
			//[['item_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArchiveUnit::className(), 'targetAttribute' => ['item_unit_id' => 'id']],
			[['fond_id'], 'exist', 'skipOnError' => true, 'targetClass' => self::className(), 'targetAttribute' => ['fond_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_FOND] = ['publish', 'archive_name', 'fond_code', 'fond_year'];
		$scenarios[self::SCENARIO_ITEM] = ['publish', 'archive_name', 'fond_id', 'item_define', 'item_type_id', 'item_unit_id', 'item_count', 'subject', 'function', 'backToManage'];
		return $scenarios;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'publish' => Yii::t('app', 'Publish'),
			'fond_id' => Yii::t('app', 'Archive List'),
			'archive_name' => Yii::t('app', 'Problem Description'),
			'fond_code' => Yii::t('app', 'Nickname'),
			'fond_year' => Yii::t('app', 'Year'),
			'item_define' => Yii::t('app', 'Definitive Number'),
			'item_type_id' => Yii::t('app', 'Type'),
			'item_unit_id' => Yii::t('app', 'Unit'),
			'item_count' => Yii::t('app', 'Archive'),
			'item_preview' => Yii::t('app', 'Preview'),
			'item_file' => Yii::t('app', 'Document'),
			'creation_date' => Yii::t('app', 'Creation Date'),
			'creation_id' => Yii::t('app', 'Creation'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'updated_date' => Yii::t('app', 'Updated Date'),
			'old_item_preview' => Yii::t('app', 'Old Preview'),
			'old_item_file' => Yii::t('app', 'Old File'),
			'fondName' => Yii::t('app', 'Archive List'),
			'typeName' => Yii::t('app', 'Type'),
			'unitName' => Yii::t('app', 'Unit'),
			'creationDisplayname' => Yii::t('app', 'Creation'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
			'archives' => Yii::t('app', 'Archives'),
			'preview' => Yii::t('app', 'Preview'),
			'file' => Yii::t('app', 'Document'),
			'location' => Yii::t('app', 'Location'),
			'subject' => Yii::t('app', 'Subject'),
			'function' => Yii::t('app', 'Function'),
			'backToManage' => Yii::t('app', 'Back to Manage'),
			'views' => Yii::t('app', 'Views'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getType()
	{
		return $this->hasOne(ArchiveType::className(), ['id' => 'item_type_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUnit()
	{
		return $this->hasOne(ArchiveUnit::className(), ['id' => 'item_unit_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreation()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'creation_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFond()
	{
		return $this->hasOne(Archives::className(), ['id' => 'fond_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getArchives($count=false, $publish=null)
	{
        if ($count == false) {
			$model = $this->hasMany(Archives::className(), ['fond_id' => 'id'])
                ->alias('archives');
            if ($publish != null) {
                $model->andOnCondition([sprintf('%s.publish', 'archives') => $publish]);
            } else {
                $model->andOnCondition(['IN', sprintf('%s.publish', 'archives'), [0,1]]);
            }

            return $model;
        }

		$model = Archives::find()
            ->alias('t')
            ->where(['t.fond_id' => $this->id]);
        if ($publish != null) {
            if ($publish == 0) {
                $model->unpublish();
            } else if ($publish == 1) {
                $model->published();
            } else if ($publish == 2) {
                $model->deleted();
            }
		} else {
            $model->andWhere(['IN', 'publish', [0,1]]);
        }
		$archives = $model->count();

		return $archives ? $archives : 0;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLocations($relation=true)
	{
        if ($relation == false) {
            return !empty($this->locations) ? $this->locations[0] : null;
        }

		return $this->hasMany(ArchiveLocations::className(), ['archive_id' => 'id'])
			->alias('locations');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSubjects($result=false, $val='id')
	{
        if ($result == true) {
            return \yii\helpers\ArrayHelper::map($this->subjects, 'tag_id', $val=='id' ? 'id' : 'tag.body');
        }

		return $this->hasMany(ArchiveSubject::className(), ['archive_id' => 'id'])
			->alias('subjects')
			->andOnCondition([sprintf('%s.type', 'subjects') => 'subject']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFunctions($result=false, $val='id')
	{
        if ($result == true) {
            return \yii\helpers\ArrayHelper::map($this->functions, 'tag_id', $val=='id' ? 'id' : 'tag.body');
        }

		return $this->hasMany(ArchiveSubject::className(), ['archive_id' => 'id'])
			->alias('functions')
			->andOnCondition([sprintf('%s.type', 'functions') => 'function']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getViews($count=false, $publish=1)
	{
        if ($count == false) {
            return $this->hasMany(ArchiveViews::className(), ['archive_id' => 'id'])
                ->alias('views')
                ->andOnCondition([sprintf('%s.publish', 'views') => $publish]);
        }

		$model = ArchiveViews::find()
            ->alias('t')
            ->where(['t.archive_id' => $this->id]);
        if ($publish == 0) {
            $model->unpublish();
        } else if ($publish == 1) {
            $model->published();
        } else if ($publish == 2) {
            $model->deleted();
        }
		$views = $model->sum('views');

		return $views ? $views : 0;
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\query\Archives the active query used by this AR class.
	 */
	public static function find()
	{
		return new \ommu\archive\models\query\Archives(get_called_class());
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['fondName'] = [
			'attribute' => 'fondName',
			'value' => function($model, $key, $index, $column) {
				return isset($model->fond) ? $model->fond->archive_name : '-';
				// return $model->fondName;
			},
			'visible' => $this->isArchive && !Yii::$app->request->get('fond') ? true : false,
		];
		$this->templateColumns['item_type_id'] = [
			'attribute' => 'item_type_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->type) ? $model->type->type_name : '-';
				// return $model->typeName;
			},
			'filter' => ArchiveType::getType(),
			'contentOptions' => ['class' => 'text-nowrap'],
			'visible' => $this->isArchive && !Yii::$app->request->get('type') ? true : false,
		];
		$this->templateColumns['archive_name'] = [
			'attribute' => 'archive_name',
			'label' => $this->isArchive ? Yii::t('app', 'Problem Description') : Yii::t('app', 'Title'),
			'value' => function($model, $key, $index, $column) {
				return $model->archive_name;
			},
			'format' => 'html',
		];
		$this->templateColumns['fond_code'] = [
			'attribute' => 'fond_code',
            'label' => !$this->isArchive ? Yii::t('app', 'Nickname') : Yii::t('app', 'Code Access'),
			'value' => function($model, $key, $index, $column) {
                if (!$model->isArchive) {
                    return $model->fond_code;
                } else {
                    $uploadPathPreview = join('/', [$model::getUploadPath(false), ($model->isNewFile ? $model->fond_id : 'fileakses')]);

                    $fondCode = $model->fond_code;
    
                    $uploadPath = join('/', [$model::getUploadPath(), ($model->isNewFile ? $model->fond_id : 'fileakses')]);
                    $fileExists = $model->item_file != '' && file_exists(join('/', [$uploadPath, $model->item_file])) ? true : false;
    
                    // if ($model->item_file && $fileExists) {
                        $fondCode = join('', [$fondCode, '<span class="glyphicon glyphicon-download ml-2"></span>']);
                    // }
    
                    return $model->item_file ? Html::a($fondCode, Url::to(join('/', ['@webpublic', $uploadPathPreview, $model->item_file])), ['title' => Yii::t('app', 'Download Archive'), 'target' => '_blank', 'data-pjax' => 0]) : '-';
                }
			},
			'contentOptions' => ['class' => 'text-center text-nowrap'],
			'format' => 'raw',
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['fond_year'] = [
			'attribute' => 'fond_year',
			'value' => function($model, $key, $index, $column) {
				return $model->fond_year;
			},
			'visible' => !$this->isArchive ? true : false,
		];
		$this->templateColumns['item_define'] = [
			'attribute' => 'item_define',
			'value' => function($model, $key, $index, $column) {
				return $model->item_define;
			},
			'contentOptions' => ['class' => 'text-center'],
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['item_count'] = [
			'attribute' => 'item_count',
			'value' => function($model, $key, $index, $column) {
                if ($model->item_count) {
                    if (isset($model->unit)) {
                        return Yii::t('app', '{count} {unit}', ['count' => $model->item_count, 'unit' => $model->unit->unit_name]);
                    }
					return $model->item_count;
				}
				return '-';
			},
			'contentOptions' => ['class' => 'text-center'],
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['item_unit_id'] = [
			'attribute' => 'item_unit_id',
			'value' => function($model, $key, $index, $column) {
				return isset($model->unit) ? $model->unit->unit_name : '-';
				// return $model->unitName;
			},
			'filter' => ArchiveUnit::getUnit(),
			'visible' => $this->isArchive && !Yii::$app->request->get('unit') ? true : false,
		];
		$this->templateColumns['subject'] = [
			'attribute' => 'subject',
			'value' => function($model, $key, $index, $column) {
				return self::parseSubject($model->getSubjects(true, 'title'), 'subjectId', ', ');
			},
			'format' => 'html',
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['function'] = [
			'attribute' => 'function',
			'value' => function($model, $key, $index, $column) {
				return self::parseSubject($model->getFunctions(true, 'title'), 'functionId', ', ');
			},
			'format' => 'html',
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['item_preview'] = [
			'attribute' => 'item_preview',
			'value' => function($model, $key, $index, $column) {
                if (!$model->item_preview) {
                    return '-';
                }

				$uploadPath = join('/', [$model::getUploadPath(false), $model->fond_id]);
				return $model->item_preview ? Html::a($model->item_preview, Url::to(join('/', ['@webpublic', $uploadPath, $model->item_preview])), ['alt' => $model->item_preview]) : '-';
			},
			'format' => 'html',
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['archives'] = [
			'attribute' => 'archives',
			'value' => function($model, $key, $index, $column) {
				$archives = $model->getArchives(true);
				return Html::a($archives, ['admin/manage', 'fond' => $model->primaryKey], ['title' => Yii::t('app', '{count} archives', ['count' => $archives]), 'data-pjax' => 0]);
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !$this->isArchive ? true : false,
		];
		$this->templateColumns['views'] = [
			'attribute' => 'views',
			'value' => function($model, $key, $index, $column) {
				$views = $model->getViews(true);
				return Html::a($views, ['view/admin/manage', 'archive' => $model->primaryKey, 'publish' => 1], ['title' => Yii::t('app', '{count} views', ['count' => $views]), 'data-pjax' => 0]);
			},
			'filter' => false,
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
		];
		$this->templateColumns['preview'] = [
			'attribute' => 'preview',
			'value' => function($model, $key, $index, $column) {
				return $this->filterYesNo($model->preview);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['file'] = [
			'attribute' => 'file',
			'value' => function($model, $key, $index, $column) {
				return $this->filterYesNo($model->file);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'visible' => $this->isArchive ? true : false,
		];
		$this->templateColumns['location'] = [
			'attribute' => 'location',
			'value' => function($model, $key, $index, $column) {
				return $this->filterYesNo($model->location);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['creation_date'] = [
			'attribute' => 'creation_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->creation_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'creation_date'),
		];
		$this->templateColumns['creationDisplayname'] = [
			'attribute' => 'creationDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->creation) ? $model->creation->displayname : '-';
				// return $model->creationDisplayname;
			},
			'visible' => !Yii::$app->request->get('creation') ? true : false,
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
		$this->templateColumns['updated_date'] = [
			'attribute' => 'updated_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->updated_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'updated_date'),
		];
		$this->templateColumns['publish'] = [
			'attribute' => 'publish',
			'value' => function($model, $key, $index, $column) {
				$url = Url::to(['publish', 'id' => $model->primaryKey]);
				return $this->quickAction($url, $model->publish);
			},
			'filter' => $this->filterYesNo(),
			'contentOptions' => ['class' => 'text-center'],
			'format' => 'raw',
			'visible' => !Yii::$app->request->get('trash') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($id, $column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => $id])->one();
            return is_array($column) ? $model : $model->$column;

        } else {
            $model = self::findOne($id);
            return $model;
        }
	}

	/**
	 * @param returnAlias set true jika ingin kembaliannya path alias atau false jika ingin string
	 * relative path. default true.
	 */
	public static function getUploadPath($returnAlias=true)
	{
		return ($returnAlias ? Yii::getAlias('@public/archive') : 'archive');
	}

	/**
	 * function getIsNewFile
	 */
	public function getIsNewFile(): bool
	{
        if ($this->_migrate == 1) {
            return false;
        }
		
		return true;
	}

	/**
	 * function getType
	 */
	public static function getBooks($publish=null, $array=true)
	{
		$model = self::find()
            ->alias('t')
			->select(['t.id', 't.archive_name', 't.fond_code']);
        if ($publish != null) {
            $model->where(['t.publish' => $publish]);
        }
		$model = $model->andWhere(['is', 't.fond_id', null])
			->orderBy('t.fond_code ASC')->all();

        if ($array == true) {
			$items = [];
			foreach ($model as $val) {
				// $items[$val->id] = [
				// 	'label' => $val->archive_name,
				// 	'code' => $val->fond_code,
				// ];
				$items[$val->id] = $val->fond_code.', '.$val->archive_name;
			}
			return $items;
		}

		return $model;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getEnableField()
	{
        if (!$this->isArchive) {
            return [];
        }

		return ArchiveSetting::getInfo('field');
	}

	/**
	 * {@inheritdoc}
	 */
	public function setArchiveSubject($type='subject')
	{
        if ($type == 'subject') {
			$oldSubject = $this->getSubjects(true, 'title');
            if ($this->subject) {
                $subject = explode(',', $this->subject);
            }
		} else {
			$oldSubject = $this->getFunctions(true, 'title');
            if ($this->function) {
                $subject = explode(',', $this->function);
            }
		}

		// insert difference subject
        if (is_array($subject)) {
			foreach ($subject as $val) {
                if (in_array($val, $oldSubject)) {
					unset($oldSubject[array_keys($oldSubject, $val)[0]]);
					continue;
				}

				$subjectFind = CoreTags::find()
					->select(['tag_id'])
					->andWhere(['body' => Inflector::camelize($val)])
					->one();

                if ($subjectFind != null) {
                    $tag_id = $subjectFind->tag_id;
                } else {
					$model = new CoreTags();
					$model->body = $val;
                    if ($model->save()) {
                        $tag_id = $model->tag_id;
                    }
				}

				$model = new ArchiveSubject();
				$model->type = $type;
				$model->archive_id = $this->id;
				$model->tag_id = $tag_id;
				$model->save();
			}
		}

		// drop difference subject
        if (!empty($oldSubject)) {
			foreach ($oldSubject as $key => $val) {
				ArchiveSubject::find()
					->select(['id'])
					->where(['type' => $type, 'archive_id' => $this->id, 'tag_id' => $key])
					->one()
					->delete();
			}
		}
	}

	/**
	 * function parseLocation
	 */
	public static function parseLocation($model)
	{
        if ($model == null) {
            return '-';
        }

        if (isset($model->rack)) {
            $items[] = Yii::t('app', 'Rack: {rack}', ['rack' => $model->rack->location_name]);
        }
        if (isset($model->room)) {
            $items[] = Yii::t('app', 'Location: {room}, {depo}, {building}', ['room' => $model->room->location_name, 'depo' => $model->depo->location_name, 'building' => $model->building->location_name]);
        }
        if (isset($model->storage)) {
            $items[] = Yii::t('app', 'Storage: {storage-name}', ['storage-name' => $model->storage->storage_name_i]);
        }
        if ($model->weight != '') {
            $items[] = Yii::t('app', 'Weight: {weight}', ['weight' => $model->weight]);
        }
        if ($model->location_desc != '') {
            $items[] = Yii::t('app', 'Noted: {location-desc}', ['location-desc' => $model->location_desc]);
        }

		return Html::ul($items, ['encode' => false, 'class' => 'list-boxed']);
	}

	/**
	 * function parseSubject
	 */
	public static function parseSubject($subjects, $attr='subjectId', $sep='li')
	{
        if (!is_array($subjects) || (is_array($subjects) && empty($subjects))) {
            return '-';
        }

		$items = [];
		foreach ($subjects as $key => $val) {
			$items[$val] = Html::a($val, ['admin/manage', $attr => $key], ['title' => $val]);
		}

        if ($sep == 'li') {
			return Html::ul($items, ['item' => function($item, $index) {
				return Html::tag('li', $item);
			}, 'class' => 'list-boxed']);
		}

		return implode($sep, $items);
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		$this->old_item_preview = $this->item_preview;
		$this->old_item_file = $this->item_file;
		// $this->fondName = isset($model->fond) ? $model->fond->archive_name : '-';
		// $this->typeName = isset($this->type) ? $this->type->type_name : '-';
		// $this->unitName = isset($this->unit) ? $this->unit->unit_name : '-';
		// $this->creationDisplayname = isset($this->creation) ? $this->creation->displayname : '-';
		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';

		$this->preview = $this->item_preview != '' ? 1 : 0;
		$this->file = $this->item_file != '' ? 1 : 0;
		$this->location = $this->getLocations(false) != null ? 1 : 0;
		$this->subject =  implode(',', $this->getSubjects(true, 'title'));
		$this->function =  implode(',', $this->getFunctions(true, 'title'));
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
			$this->scenario = self::SCENARIO_FOND;
            if ($this->isArchive) {
                $this->scenario = self::SCENARIO_ITEM;
            }

            if ($this->scenario == self::SCENARIO_ITEM) {
				$setting = ArchiveSetting::find()
					->select(['item_preview_type', 'item_file_type'])
					->where(['id' => 1])
					->one();

				// $this->item_preview = UploadedFile::getInstance($this, 'item_preview');
                if ($this->item_preview instanceof UploadedFile && !$this->item_preview->getHasError()) {
					$itemFileType = $this->formatFileType($setting->item_preview_type);
	
                    if (!in_array(strtolower($this->item_preview->getExtension()), $itemFileType)) {
						$this->addError('item_preview', Yii::t('app', 'The file {name} cannot be uploaded. Only files with these extensions are allowed: {extensions}', [
							'name' => $this->item_preview->name,
							'extensions' => $this->formatFileType($itemFileType, false),
						]));
					}
				} /* else {
                    if ($this->isNewRecord || (!$this->isNewRecord && $this->old_item_preview == '')) {
                        $this->addError('item_preview', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('item_preview')]));
                    }
				} */
	
				// $this->item_file = UploadedFile::getInstance($this, 'item_file');
                if ($this->item_file instanceof UploadedFile && !$this->item_file->getHasError()) {
					$itemFileType = $this->formatFileType($setting->item_file_type);
	
                    if (!in_array(strtolower($this->item_file->getExtension()), $itemFileType)) {
						$this->addError('item_file', Yii::t('app', 'The file {name} cannot be uploaded. Only files with these extensions are allowed: {extensions}', [
							'name' => $this->item_file->name,
							'extensions' => $this->formatFileType($itemFileType, false),
						]));
					}
				} /* else {
                    if ($this->isNewRecord || (!$this->isNewRecord && $this->old_item_file == '')) {
                        $this->addError('item_file', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('item_file')]));
                    }
				} */
			}

            if ($this->isNewRecord) {
                if ($this->creation_id == null) {
                    $this->creation_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            } else {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
            if ($this->scenario == self::SCENARIO_ITEM) {
				$setting = ArchiveSetting::find()
					->select(['item_renamed'])
					->where(['id' => 1])
					->one();

                $uploadPath = join('/', [self::getUploadPath(), $this->fond_id]);
                $verwijderenPath = join('/', [self::getUploadPath(), 'verwijderen']);
                $this->createUploadDirectory(self::getUploadPath(), $this->fond_id);

				// $this->item_preview = UploadedFile::getInstance($this, 'item_preview');
                if ($this->item_preview instanceof UploadedFile && !$this->item_preview->getHasError()) {
					$fileName = $setting->item_renamed == 1 ? 
						join('-', [time(), UuidHelper::uuid()]).'.'.strtolower($this->item_preview->getExtension()) : 
						$this->item_preview->getBaseName().'.'.strtolower($this->item_preview->getExtension());

                    if ($this->item_preview->saveAs(join('/', [$uploadPath, $fileName]))) {
                        if ($this->old_item_preview != '' && file_exists(join('/', [$uploadPath, $this->old_item_preview]))) {
                            rename(join('/', [$uploadPath, $this->old_item_preview]), join('/', [$verwijderenPath, $this->fond_id.'-'.$this->id.'-'.time().'_change_'.$this->old_item_preview]));
                        }
						$this->item_preview = $fileName;
					}
				} else {
                    if (!$insert && $this->item_preview == '') {
                        $this->item_preview = $this->old_item_preview;
                    }
				}

				// $this->item_file = UploadedFile::getInstance($this, 'item_file');
                if ($this->item_file instanceof UploadedFile && !$this->item_file->getHasError()) {
					$fileName = $setting->item_renamed == 1 ? 
						join('-', [time(), UuidHelper::uuid()]).'.'.strtolower($this->item_file->getExtension()) : 
						$this->item_file->getBaseName().'.'.strtolower($this->item_file->getExtension());

                    if ($this->item_file->saveAs(join('/', [$uploadPath, $fileName]))) {
                        if ($this->old_item_file != '' && file_exists(join('/', [$uploadPath, $this->old_item_file]))) {
                            rename(join('/', [$uploadPath, $this->old_item_file]), join('/', [$verwijderenPath, $this->fond_id.'-'.$this->id.'-'.time().'_change_'.$this->old_item_file]));
                        }
						$this->item_file = $fileName;
					}
				} else {
                    if (!$insert && $this->item_file == '') {
                        $this->item_file = $this->old_item_file;
                    }
				}

				// insert new unit
                if (!isset($this->unit)) {
					$model = new ArchiveUnit();
					$model->unit_name = $this->item_unit_id;
                    if ($model->save()) {
                        $this->item_unit_id = $model->id;
                    }
				}
		
                if (!$insert) {
					$this->setArchiveSubject();
					$this->setArchiveSubject('function');
				}
            }
        }
        return true;
	}

	/**
	 * After save attributes
	 */
	public function afterSave($insert, $changedAttributes)
	{
        parent::afterSave($insert, $changedAttributes);

        if ($this->scenario == self::SCENARIO_FOND) {
            // generate upload path
            $uploadPath = join('/', [self::getUploadPath(), $this->id]);
            $verwijderenPath = join('/', [self::getUploadPath(), 'verwijderen']);
            $this->createUploadDirectory(self::getUploadPath(), $this->id);
        }

        if ($this->scenario == self::SCENARIO_ITEM) {
            if ($insert) {
                $this->setArchiveSubject();
                $this->setArchiveSubject('function');
            }
        }
	}

	/**
	 * After delete attributes
	 */
	public function afterDelete()
	{
        parent::afterDelete();

        if ($this->scenario == self::SCENARIO_ITEM) {
			$uploadPath = join('/', [self::getUploadPath(), $this->fond_id]);
			$verwijderenPath = join('/', [self::getUploadPath(), 'verwijderen']);

            if ($this->item_preview != '' && file_exists(join('/', [$uploadPath, $this->item_preview]))) {
                rename(join('/', [$uploadPath, $this->item_preview]), join('/', [$verwijderenPath, $this->fond_id.'-'.$this->id.'-'.time().'_deleted_'.$this->item_preview]));
            }

            if ($this->item_file != '' && file_exists(join('/', [$uploadPath, $this->item_file]))) {
                rename(join('/', [$uploadPath, $this->item_file]), join('/', [$verwijderenPath, $this->fond_id.'-'.$this->id.'-'.time().'_deleted_'.$this->item_file]));
            }
		}
	}
}
