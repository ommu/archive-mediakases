<?php
/**
 * ArchiveType
 *
 * This is the ActiveQuery class for [[\ommu\archive\models\ArchiveType]].
 * @see \ommu\archive\models\ArchiveType
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 3 October 2019, 16:01 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\models\query;

class ArchiveType extends \yii\db\ActiveQuery
{
	/*
	public function active()
	{
		return $this->andWhere('[[status]]=1');
	}
	*/

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveType[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveType|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
