<?php
/**
 * ArchiveViewHistory
 *
 * This is the ActiveQuery class for [[\ommu\archive\models\ArchiveViewHistory]].
 * @see \ommu\archive\models\ArchiveViewHistory
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 12 Fabruary 2020, 08:42 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\models\query;

class ArchiveViewHistory extends \yii\db\ActiveQuery
{
	/*
	public function active()
	{
		return $this->andWhere('[[status]]=1');
	}
	*/

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveViewHistory[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveViewHistory|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
