<?php
/**
 * ArchiveSubject
 *
 * This is the ActiveQuery class for [[\ommu\archive\models\ArchiveSubject]].
 * @see \ommu\archive\models\ArchiveSubject
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 9 February 2020, 23:45 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 */

namespace ommu\archive\models\query;

class ArchiveSubject extends \yii\db\ActiveQuery
{
	/*
	public function active()
	{
		return $this->andWhere('[[status]]=1');
	}
	*/

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveSubject[]|array
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \ommu\archive\models\ArchiveSubject|array|null
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}
}
