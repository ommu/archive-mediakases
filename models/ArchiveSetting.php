<?php
/**
 * ArchiveSetting
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 06 October 2019, 22:16 WIB
 * @link https://bitbucket.org/ommu/archive-mediakases
 *
 * This is the model class for table "ommu_archive_setting".
 *
 * The followings are the available columns in table "ommu_archive_setting":
 * @property integer $id
 * @property string $license
 * @property integer $permission
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $field
 * @property string $item_preview_type
 * @property string $item_file_type
 * @property integer $item_renamed
 * @property string $breadcrumb_param
 * @property string $modified_date
 * @property integer $modified_id
 *
 * The followings are the available model relations:
 * @property Users $modified
 *
 */

namespace ommu\archive\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Users;
use yii\helpers\Json;

class ArchiveSetting extends \app\components\ActiveRecord
{
	use \ommu\traits\UtilityTrait;
	use \ommu\traits\FileTrait;

	public $gridForbiddenColumn = [];

	public $breadcrumb;

	public $modifiedDisplayname;

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'ommu_archive_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			[['license', 'permission', 'meta_description', 'meta_keyword', 'item_preview_type', 'item_file_type', 'item_renamed', 'breadcrumb_param'], 'required'],
			[['permission', 'item_renamed', 'modified_id'], 'integer'],
			[['meta_description', 'meta_keyword'], 'string'],
			//[['field', 'item_preview_type', 'item_file_type', 'breadcrumb_param], 'json'],
			[['field'], 'safe'],
			[['license'], 'string', 'max' => 32],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'license' => Yii::t('app', 'License'),
			'permission' => Yii::t('app', 'Permission'),
			'meta_description' => Yii::t('app', 'Meta Description'),
			'meta_keyword' => Yii::t('app', 'Meta Keyword'),
			'field' => Yii::t('app', 'Custom Field'),
			'item_preview_type' => Yii::t('app', 'Preview File Type'),
			'item_file_type' => Yii::t('app', 'Document File Type'),
			'item_renamed' => Yii::t('app', 'Renamed Filename'),
			'breadcrumb_param' => Yii::t('app', 'Breadcrumb Param'),
			'modified_date' => Yii::t('app', 'Modified Date'),
			'modified_id' => Yii::t('app', 'Modified'),
			'modifiedDisplayname' => Yii::t('app', 'Modified'),
			'breadcrumb' => Yii::t('app', 'Breadcrumb Apps'),
			'breadcrumb_status' => Yii::t('app', 'Breadcrumb Apps Status'),
			'breadcrumb_app' => Yii::t('app', 'Breadcrumb Apps Name and URL'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getModified()
	{
		return $this->hasOne(Users::className(), ['user_id' => 'modified_id']);
	}

	/**
	 * Set default columns to display
	 */
	public function init()
	{
        parent::init();

        if (!(Yii::$app instanceof \app\components\Application)) {
            return;
        }

        if (!$this->hasMethod('search')) {
            return;
        }

		$this->templateColumns['_no'] = [
			'header' => '#',
			'class' => 'app\components\grid\SerialColumn',
			'contentOptions' => ['class' => 'text-center'],
		];
		$this->templateColumns['license'] = [
			'attribute' => 'license',
			'value' => function($model, $key, $index, $column) {
				return $model->license;
			},
		];
		$this->templateColumns['permission'] = [
			'attribute' => 'permission',
			'value' => function($model, $key, $index, $column) {
				return self::getPermission($model->permission);
			},
		];
		$this->templateColumns['meta_description'] = [
			'attribute' => 'meta_description',
			'value' => function($model, $key, $index, $column) {
				return $model->meta_description;
			},
		];
		$this->templateColumns['meta_keyword'] = [
			'attribute' => 'meta_keyword',
			'value' => function($model, $key, $index, $column) {
				return $model->meta_keyword;
			},
		];
		$this->templateColumns['field'] = [
			'attribute' => 'field',
			'value' => function($model, $key, $index, $column) {
				return $model::getField($model->field, ',');
			},
		];
		$this->templateColumns['item_preview_type'] = [
			'attribute' => 'item_preview_type',
			'value' => function($model, $key, $index, $column) {
				return $model->item_preview_type;
			},
		];
		$this->templateColumns['item_file_type'] = [
			'attribute' => 'item_file_type',
			'value' => function($model, $key, $index, $column) {
				return $model->item_file_type;
			},
		];
		$this->templateColumns['item_renamed'] = [
			'attribute' => 'item_renamed',
			'value' => function($model, $key, $index, $column) {
				return $model::getBreadcrumbStatus($model->item_renamed);
			},
		];
		$this->templateColumns['breadcrumb_param'] = [
			'attribute' => 'breadcrumb_param',
			'value' => function($model, $key, $index, $column) {
				return $model::parseBreadcrumbApps($model->breadcrumb_param);
			},
		];
		$this->templateColumns['modified_date'] = [
			'attribute' => 'modified_date',
			'value' => function($model, $key, $index, $column) {
				return Yii::$app->formatter->asDatetime($model->modified_date, 'medium');
			},
			'filter' => $this->filterDatepicker($this, 'modified_date'),
		];
		$this->templateColumns['modifiedDisplayname'] = [
			'attribute' => 'modifiedDisplayname',
			'value' => function($model, $key, $index, $column) {
				return isset($model->modified) ? $model->modified->displayname : '-';
				// return $model->modifiedDisplayname;
			},
			'visible' => !Yii::$app->request->get('modified') ? true : false,
		];
	}

	/**
	 * User get information
	 */
	public static function getInfo($column=null)
	{
        if ($column != null) {
            $model = self::find();
            if (is_array($column)) {
                $model->select($column);
            } else {
                $model->select([$column]);
            }
            $model = $model->where(['id' => 1])->one();
            return is_array($column) ? $model : $model->$column;
			
		} else {
			$model = self::findOne(1);
			return $model;
		}
	}

	/**
	 * function getPermission
	 */
	public static function getPermission($value=null)
	{
		$moduleName = "module name";
		$module = strtolower(Yii::$app->controller->module->id);
        if (($module = Yii::$app->moduleManager->getModule($module)) != null) {
            $moduleName = strtolower($module->getName());
        }

		$items = array(
			1 => Yii::t('app', 'Yes, the public can view {module} unless they are made private.', ['module' => $moduleName]),
			0 => Yii::t('app', 'No, the public cannot view {module}.', ['module' => $moduleName]),
		);

        if ($value !== null) {
            return $items[$value];
        } else {
            return $items;
        }
	}

	/**
	 * function getField
	 */
	public static function getField($field=null, $sep='li')
	{
		$items = array(
			'subject' => Yii::t('app', 'Subject'),
			'function' => Yii::t('app', 'Function'),
		);

        if ($field !== null) {
            if (!is_array($field) || (is_array($field) && empty($field))) {
                return '-';
            }

			$item = [];
			foreach ($items as $key => $val) {
                if (in_array($key, $field)) {
                    $item[$key] = $val;
                }
			}

            if ($sep == 'li') {
				return Html::ul($item, ['item' => function($item, $index) {
					return Html::tag('li', "($index) $item");
				}, 'class' => 'list-boxed']);
			}

			return implode(', ', $item);
		} else
			return $items;
	}

	/**
	 * function getBreadcrumbStatus
	 */
	public static function getBreadcrumbStatus($value=null)
	{
		$items = array(
			'1' => Yii::t('app', 'Enable'),
			'0' => Yii::t('app', 'Disable'),
		);

        if ($value !== null) {
            return $items[$value];
        } else {
            return $items;
        }
	}

	/**
	 * function getBreadcrumbApps
	 */
	public function getBreadcrumbApps(): bool
	{
        if (!is_array($this->breadcrumb_param))
			return false;

        if ($this->breadcrumb_param['status'] != 1) {
            return false;
        }

		unset($this->breadcrumb_param['status']);
        if (!($this->breadcrumb_param['name'] != '' && $this->breadcrumb_param['url'] != '')) {
            return false;
        }

		return true;
	}

	/**
	 * function getBreadcrumbAppParam
	 */
	public function getBreadcrumbAppParam()
	{
        if (!$this->getBreadcrumbApps()) {
            return [];
        }

		$params = $this->breadcrumb_param;
		unset($params['status']);
		return $params;
	}

	/**
	 * function parseBreadcrumbApps
	 */
	public static function parseBreadcrumbApps($params)
	{
        if (!empty($params)) {
            unset($params['status']);
        }

        if ($params == null) {
            return '-';
        }

		return Html::ul($params, ['encode' => false, 'class' => 'list-boxed']);
	}

	/**
	 * after find attributes
	 */
	public function afterFind()
	{
		parent::afterFind();

		$field = Json::decode($this->field);
		$this->field = is_array($field) && !empty($field) ? $field : [];

		$item_preview_type = Json::decode($this->item_preview_type);
        if (!empty($item_preview_type)) {
            $this->item_preview_type = $this->formatFileType($item_preview_type, false);
        }

		$item_file_type = Json::decode($this->item_file_type);
        if (!empty($item_file_type)) {
            $this->item_file_type = $this->formatFileType($item_file_type, false);
        }

        if ($this->breadcrumb_param == '') {
            $breadcrumb_param = [];
        } else {
            $breadcrumb_param = Json::decode($this->breadcrumb_param);
        }
        if (!empty($breadcrumb_param)) {
            $this->breadcrumb_param = $breadcrumb_param;
        }

		$this->breadcrumb = $this->getBreadcrumbApps() ? 1 : 0;

		// $this->modifiedDisplayname = isset($this->modified) ? $this->modified->displayname : '-';
	}

	/**
	 * before validate attributes
	 */
	public function beforeValidate()
	{
        if (parent::beforeValidate()) {
            if (!$this->isNewRecord) {
                if ($this->modified_id == null) {
                    $this->modified_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
                }
            }
            if ($this->breadcrumb_param['status'] == '') {
                $this->addError('breadcrumb_param', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('breadcrumb_status')]));
            } else {
                if ($this->breadcrumb_param['status'] == 1 && !$this->getBreadcrumbApps()) {
                    $this->addError('breadcrumb_param', Yii::t('app', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel('breadcrumb_app')]));
                }
            }
        }
        return true;
	}

	/**
	 * before save attributes
	 */
	public function beforeSave($insert)
	{
        if (parent::beforeSave($insert)) {
			$this->field = Json::encode($this->field);
			$this->item_preview_type = Json::encode($this->formatFileType($this->item_preview_type));
			$this->item_file_type = Json::encode($this->formatFileType($this->item_file_type));
			$this->breadcrumb_param = Json::encode($this->breadcrumb_param);
        }
        return true;
	}
}
